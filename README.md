# Template tesi Dip. Informatica @ Unimi

Fork del 2022 mio con fix e layout changes nel frontespizio (molto soggettivi ovviamente, se volete potete sempre usare il layout vecchio)


Molto prototipale, mancano ancora tanti package interessanti/utili da includere

NOTA BENE: in generale valga la regola "PLEASE DON'T GIT BUILDABLE FILES!"

Se volete un'anteprima del template andate [qui](https://gitlab.com/kobimex/TemplateTesi2022/-/pipelines) e premete i tre puntini a destra per scaricare lo zip della build CI/CD

**Questo progetto non è in alcun modo affiliato all'Università degli Studi di Milano Statale**

## TODO
- list used/required packages for installs that can't use makefiles
- makefile or sh scirpt for linux (only debian probs)
- docker deployement using images such as listx/texlive
- include other useful packages
